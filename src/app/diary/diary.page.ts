import {Component, OnInit, ViewChild} from '@angular/core';
import {DiaryEvent, Test} from '../shared/interfaces';
import {CalendarComponent} from 'ionic2-calendar/calendar';
import {ModalController} from '@ionic/angular';
import {DiaryService} from '../shared/services/diary.service';
import {Observable} from 'rxjs';
import {DiaryModalComponent} from './diary-modal/diary-modal.component';

@Component({
  selector: 'app-diary',
  templateUrl: 'diary.page.html',
  styleUrls: ['diary.page.scss']
})
export class DiaryPage implements OnInit {
  @ViewChild(CalendarComponent, null) diaryCalendar: CalendarComponent;

  eventSource$: Observable<DiaryEvent[]>;

  calendar = {
    mode: 'month',
    currentDate: new Date()
  };

  calendarTitle: string;

  constructor(
    private diaryService: DiaryService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.eventSource$ = this.diaryService.getAll();
  }

  onViewTitleChanged(title) {
    this.calendarTitle = title;
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: DiaryModalComponent
    });
    return await modal.present();
  }
}
