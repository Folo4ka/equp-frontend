import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {DiaryEmotion} from '../../shared/interfaces';
import {DiaryService} from '../../shared/services/diary.service';

@Component({
  selector: 'app-diary-modal',
  templateUrl: './diary-modal.component.html',
  styleUrls: ['./diary-modal.component.scss'],
})
export class DiaryModalComponent implements OnInit {

  emotions$: Observable<DiaryEmotion[]>;

  constructor(
    private diaryService: DiaryService,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.emotions$ = this.diaryService.getEmotions();
  }

  dismissModal() {
    this.modalController.dismiss();
  }

  onSelectEmotion(event) {
    this.diaryService.postEmotion(event.detail.value).subscribe(() => {
        this.dismissModal();
    });

    this.dismissModal();
  }
}
