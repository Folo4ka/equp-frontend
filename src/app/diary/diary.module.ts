import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DiaryPage } from './diary.page';
import {NgCalendarModule} from 'ionic2-calendar';
import {DiaryModalComponent} from './diary-modal/diary-modal.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: DiaryPage }]),
    NgCalendarModule
  ],
  declarations: [DiaryPage, DiaryModalComponent],
  entryComponents: [DiaryModalComponent]
})
export class DiaryModule {}
