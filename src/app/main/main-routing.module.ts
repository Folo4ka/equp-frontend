import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Main } from './main';

const routes: Routes = [
  {
    path: '',
    component: Main,
    children: [
      // TODO: add default mainpage (dashboard)
      {
        path: '',
        redirectTo: '/tests/list',
        pathMatch: 'full'
      },
      {
        path: 'tests',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tests/tests.module').then(m => m.TestsModule)
          }
        ]
      },
      {
        path: 'exercise',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../exercise/exercise.module').then(m => m.ExerciseModule)
          }
        ]
      },
      {
        path: 'blog',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../blog/blog.module').then(m => m.BlogPageModule)
          }
        ]
      },
      {
        path: 'diary',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../diary/diary.module').then(m => m.DiaryModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../profile/profile.module').then(m => m.ProfilePageModule)
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
