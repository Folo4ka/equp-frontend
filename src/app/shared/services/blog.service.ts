import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article, Lifehack} from '../interfaces';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  lhGetAll(): Observable<Lifehack[]> {
    return this.http.get<Lifehack[]>(`${environment.apiUrl}/lifehacks/all/`);
  }

  articleGetAll(): Observable<Article[]> {
    return this.http.get<Article[]>(`${environment.apiUrl}/articles/all/`);
  }

  articleGetById(id: string): Observable<Article> {
    return this.http.get<Article>(`${environment.apiUrl}/articles/${id}/`);
  }
}
