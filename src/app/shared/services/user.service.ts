import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../interfaces';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  // always need 1, backend find real user from "Authorization" header in request
  getUser(): Observable<User> {
    console.log(this);
    return this.http.get<User>(`${environment.apiUrl}/accounts/users/1/`);
  }

  update(user: User): Observable<User> {
    return this.http.patch<User>(`${environment.apiUrl}/accounts/users/1/`, user);
  }
}
