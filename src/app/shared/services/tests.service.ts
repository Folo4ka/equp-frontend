import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Test, TestAnswerJson, TestResult, TestResultMeta} from '../interfaces';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestsService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Test[]> {
    return this.http.get<Test[]>(`${environment.apiUrl}/test/all/`);
    // return this.http.get<Test[]>('assets/data/test-list.json');
  }

  getById(id: string): Observable<Test> {
    return this.http.get<Test>(`${environment.apiUrl}/test/${id}/`);
    // return this.http.get<Test>('assets/data/test-item.json');
  }

  /** post test answers and return test result ID
   * @param answers - test tasks answers array.
   * @param id - test ID.
   * @return test result ID.
   */
  postAnswers(answers: TestAnswerJson, id: number): Observable<any> {
    return this.http.post<number>(`${environment.apiUrl}/test/${id}/answers/`, answers);
  }

  getResultsAll() {
    return this.http.get<TestResultMeta[]>(`${environment.apiUrl}/test/results/all/`);
  }

  getResultById(id: string): Observable<TestResult> {
    return this.http.get<TestResult>(`${environment.apiUrl}/test/results/${id}/`);
  }
}
