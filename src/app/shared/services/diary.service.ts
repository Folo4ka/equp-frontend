import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DiaryEmotion, DiaryEvent, Test, TestAnswer} from '../interfaces';
import {map} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DiaryService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<DiaryEvent[]> {
    return this.http.get<DiaryEvent[]>(`${environment.apiUrl}/diary/all/`)
      .pipe(map((response: any) => {
        const diaryEventsArr = response;
        diaryEventsArr.map((diaryEvent) => {
          diaryEvent.startTime = new Date(diaryEvent.start_time);
          diaryEvent.endTime = new Date(diaryEvent.end_time);
          delete diaryEvent.start_time;
          delete diaryEvent.end_time;
        });

        return diaryEventsArr;
      }));
  }

  getEmotions(): Observable<DiaryEmotion[]> {
    return this.http.get<DiaryEmotion[]>(`${environment.apiUrl}/emotions/all/`);
  }

  /** post emotion object and return request status.
   * @param id - emotion ID.
   * @return request status.
   */
  postEmotion(id: string): Observable<any> {
    const emotionObj = {
      id,
      date: new Date().toISOString()
    };
    return this.http.post<number>(`${environment.apiUrl}/diary/add/`, emotionObj);
  }
}
