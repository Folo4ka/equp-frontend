import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {User} from '../interfaces';
import {environment} from '../../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import {Subject, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public error$: Subject<Array<string>> = new Subject<Array<string>>();

  get token() {
    // TODO: check token for expire and clear storage, backend dont do this now
    return localStorage.getItem('userToken');
  }

  constructor(private http: HttpClient) { }

  login(user: User) {
    return this.http.post<User>(`${environment.apiUrl}/accounts/token/`, user)
      .pipe(
        tap(this.setToken),
        catchError(this.authErrorHandler.bind(this))
      );
  }

  logout() {
    this.setToken(null);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  register(user: User) {
    return this.http.post<User>(`${environment.apiUrl}/accounts/registration/`, user)
      .pipe(
        catchError(this.authErrorHandler.bind(this))
      );
  }

  private setToken(response: any | null) {
    if (response) {
      console.log(response);
      localStorage.setItem('userToken', response.token);
      // TODO add expire date for token in API
      // localStorage.setItem('userTokenExpireDate', response.expire);
    } else {
      localStorage.clear();
    }
  }

  private authErrorHandler(error: HttpErrorResponse) {
    const errorMessages = error.error;
    const messageArr = [];

    Object.keys(errorMessages).forEach((key) => {
      if (key === 'username') { // hack for django API required username field
        messageArr.push('This email already exist.');
      } else {
        messageArr.push(errorMessages[key][0]);
      }
    });
    this.error$.next(messageArr);
    return throwError(error);
  }
}
