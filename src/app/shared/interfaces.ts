export interface User {
  id?: string;
  first_name?: string;
  username: string; // same as email, django always required username
  email?: string;
  password: string;
  password2?: string;
}

export interface Test {
  id: string;
  title: string;
  imageSrc?: string;
  description: string;
  instruction: string;
  time: number;
  tasks: TestTask[];
}

export interface TestTask {
  id: string;
  type: string;
  subject: string;
  variants?: [
    {
      id: number;
      value: string;
    }
  ];
}

export interface TestAnswer {
  task_id: string;
  answer_id: string;
}

export interface TestAnswerJson {
  date: Date;
  answers: TestAnswer[];
}

export interface TestResult {
  id: string;
  title: string;
  date: string;
  description: string;
  results: [
    {
      param: string;
      value: number;
      value_label: string;
      max_value: number;
    }
  ];
}

export interface TestResultMeta {
  id: string;
  title: string;
  imageSrc?: string;
  description: string;
  date: number;
}

export interface DiaryEvent {
  id: string;
  title: string;
  description: string;
  startTime: Date;
  endTime: Date;
  allDay: boolean;
}

export interface DiaryEmotion {
  id: string;
  title: string;
}

export interface Lifehack {
  id: string;
  title?: string;
  text: string;
  imageSrc?: string;
}

export interface Article {
  id: string;
  title: string;
  description?: string;
  text?: string;
  imageSrc?: string;
}

