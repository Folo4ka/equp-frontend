import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MustMatch} from '../../shared/validators/mustMatch.validator';
import {User} from '../../shared/interfaces';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss'],
})
export class RegistrationPageComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [
        Validators.required
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8)
      ]),
      password2: new FormControl(null, [
        Validators.required,
        Validators.minLength(8)
      ])
    }, {
      validators: MustMatch('password', 'password2')
    });
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;
    const user: User = {
      first_name: this.form.value.name,
      username: this.form.value.email,
      email: this.form.value.email,
      password: this.form.value.password,
      password2: this.form.value.password2
    };

    this.auth.register(user).subscribe((value) => {
      console.log(value);
      this.submitted = false;
      this.form.reset();
      this.router.navigate(['/auth']);
    }, (error) => {
      this.submitted = false;
    });
  }

}
