import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BlogPage } from './blog.page';
import {LifehacksComponent} from './lifehacks/lifehacks.component';
import {ArticlesComponent} from './articles/articles.component';
import {RouterModule} from '@angular/router';
import {ArticleItemComponent} from './articles/article-item/article-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '', component: BlogPage, children: [
          {path: 'lifehacks', component: LifehacksComponent},
          {path: 'articles', component: ArticlesComponent},
          {path: 'articles/:id', component: ArticleItemComponent},
          {
            path: '',
            redirectTo: 'lifehacks',
            pathMatch: 'full'
          }
        ]
      }
    ])
  ],
  declarations: [
    BlogPage,
    LifehacksComponent,
    ArticlesComponent,
    ArticleItemComponent
  ]
})
export class BlogPageModule {}
