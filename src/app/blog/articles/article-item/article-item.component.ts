import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Article} from '../../../shared/interfaces';
import {BlogService} from '../../../shared/services/blog.service';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss'],
})
export class ArticleItemComponent implements OnInit {

  article$: Observable<Article>;

  constructor(
    private blogService: BlogService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.article$ = this.route.params
      .pipe(switchMap((params: Params) => {
        const articleId = params.id;
        return this.blogService.articleGetById(String(articleId));
      }));
  }

}
