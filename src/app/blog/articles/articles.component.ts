import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Article} from '../../shared/interfaces';
import {BlogService} from '../../shared/services/blog.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss'],
})
export class ArticlesComponent implements OnInit {

  articles$: Observable<Article[]>;

  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.articles$ = this.blogService.articleGetAll();
  }

}
