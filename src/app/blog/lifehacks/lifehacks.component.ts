import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {Lifehack} from '../../shared/interfaces';
import {BlogService} from '../../shared/services/blog.service';

@Component({
  selector: 'app-lifehacks',
  templateUrl: './lifehacks.component.html',
  styleUrls: ['./lifehacks.component.scss'],
})
export class LifehacksComponent implements OnInit {

  lifehacks$: Observable<Lifehack[]>;

  constructor(private blogService: BlogService) { }

  ngOnInit() {
    this.lifehacks$ = this.blogService.lhGetAll();
  }

}
