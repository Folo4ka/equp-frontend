import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {Test, TestAnswer, TestTask, TestAnswerJson} from '../../shared/interfaces';
import {TestsService} from '../../shared/services/tests.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {IonSlides} from '@ionic/angular';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-test-item',
  templateUrl: './test-item.component.html',
  styleUrls: ['./test-item.component.scss'],
})
export class TestItemComponent implements OnInit, OnDestroy {
  @ViewChild(IonSlides, {static: false}) testSlider: IonSlides;
  test$: Observable<Test>;
  testSubscription: Subscription;
  testId: number;
  testTasks: TestTask[];
  isTestRunning = false;
  currentTask = 1;
  testForm: FormGroup;

  constructor(
    private testService: TestsService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.test$ = this.route.params
      .pipe(switchMap((params: Params) => {
        this.testId = params.id;
        return this.testService.getById(String(this.testId));
      }));
    // create formGroup from api test object
    this.testSubscription = this.test$.subscribe(data => {
      const testFormControls = {};
      this.testTasks = data.tasks;
      this.testTasks.forEach(task => {
        testFormControls[`task[${task.id}]`] = new FormControl(null);
      });
      this.testForm = new FormGroup(testFormControls);
    });
  }

  ngOnDestroy() {
    this.testSubscription.unsubscribe();
  }

  startTest(): void {
    this.isTestRunning = true;
  }

  finishTest(): void {
    // grab answers to apiready json
    const testAnswers: TestAnswer[] = [];
    this.testTasks.forEach(task => {
      testAnswers.push({
        task_id: task.id,
        answer_id: this.testForm.value[`task[${task.id}]`]
      });
    });

    const testAnswersJson: TestAnswerJson = {
      date: new Date(),
      answers: testAnswers
    };

    this.testService.postAnswers(testAnswersJson, this.testId).subscribe(result => {
      // redirect to result page
      this.router.navigate(['/tests', 'results', result.id]);
    });
  }

  toPrevTask(): void {
    this.testSlider.slidePrev().then();
    this.currentTask--;
  }

  toNextTask(): void {
    if (this.currentTask !== this.testTasks.length) {
      this.currentTask++;
    }
    this.testSlider.slideNext().then();
  }

}
