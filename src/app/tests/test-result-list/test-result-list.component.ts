import { Component, OnInit } from '@angular/core';
import {TestsService} from '../../shared/services/tests.service';
import {Observable} from 'rxjs';
import {TestResultMeta} from '../../shared/interfaces';

@Component({
  selector: 'app-test-result-list',
  templateUrl: './test-result-list.component.html',
  styleUrls: ['./test-result-list.component.scss'],
})
export class TestResultListComponent implements OnInit {

  testsResults$: Observable<TestResultMeta[]>;

  constructor(private testService: TestsService) { }

  ngOnInit() {
    this.testsResults$ = this.testService.getResultsAll();
  }

}
