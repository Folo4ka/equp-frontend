import { Component, OnInit } from '@angular/core';
import {TestsService} from '../../shared/services/tests.service';
import {ActivatedRoute, Params} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {TestResult} from '../../shared/interfaces';

@Component({
  selector: 'app-test-result-item',
  templateUrl: './test-result-item.component.html',
  styleUrls: ['./test-result-item.component.scss'],
})
export class TestResultItemComponent implements OnInit {

  testResult$: Observable<TestResult>;

  constructor(private testService: TestsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.testResult$ = this.route.params
      .pipe(switchMap((params: Params) => {
        return this.testService.getResultById(String(params.id));
      }));
  }

}
