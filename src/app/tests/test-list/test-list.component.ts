import { Component, OnInit } from '@angular/core';
import {Test} from '../../shared/interfaces';
import {Observable} from 'rxjs';
import {TestsService} from '../../shared/services/tests.service';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss'],
})
export class TestListComponent implements OnInit {

  tests$: Observable<Test[]>;

  constructor(private testService: TestsService) { }

  ngOnInit() {
    this.tests$ = this.testService.getAll();
  }

}
