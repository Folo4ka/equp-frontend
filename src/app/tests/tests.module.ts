import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TestsPage } from './tests.page';
import {TestListComponent} from './test-list/test-list.component';
import {TestItemComponent} from './test-item/test-item.component';
import {TestResultListComponent} from './test-result-list/test-result-list.component';
import {TestResultItemComponent} from './test-result-item/test-result-item.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
        path: '', component: TestsPage, children: [
          {path: 'list', component: TestListComponent},
          {path: 'list/:id', component: TestItemComponent},
          {path: 'results', component: TestResultListComponent},
          {path: 'results/:id', component: TestResultItemComponent},
          {
            path: '',
            redirectTo: 'list',
            pathMatch: 'full'
          }
        ]
      }
    ])
  ],
  declarations: [
    TestsPage,
    TestListComponent,
    TestItemComponent,
    TestResultListComponent,
    TestResultItemComponent
  ]
})
export class TestsModule {}
