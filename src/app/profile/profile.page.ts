import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, Subscription} from 'rxjs';
import {User} from '../shared/interfaces';
import {Router} from '@angular/router';
import {UserService} from '../shared/services/user.service';
import {AuthService} from '../shared/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit, OnDestroy {

  user$: Observable<User>;
  dataSubscription: Subscription;
  updateSub: Subscription;
  form: FormGroup;
  submitted = false;
  message: string;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.user$ = this.userService.getUser();

    this.dataSubscription = this.user$.subscribe((data: User) => {
      this.form.patchValue({
        first_name: data.first_name,
        email: data.email
      });
    });

    this.form = new FormGroup({
      first_name: new FormControl(null, [
        Validators.required
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
    });
  }

  ngOnDestroy() {
    this.dataSubscription.unsubscribe();
  }

  submit() {
    if (this.form.invalid) {
      return;
    }

    this.submitted = true;
    console.log(this.form.value);
    this.updateSub = this.userService.update(this.form.value).subscribe(() => {
      this.submitted = false;
      alert('Profile saved!');
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth']);
  }

}
